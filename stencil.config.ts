import { Config } from '@stencil/core';
const { sass } = require('@stencil/sass');

export const config: Config = {
  namespace: 'lee-ui',
  globalStyle: 'src/global/styles/main.scss',
  outputTargets:[
    { type: 'dist' },
    { type: 'docs' },
    {
      type: 'www',
      baseUrl: '/lee-ui',
      serviceWorker: null // disable service workers
    }
  ],
  plugins:[
    sass()
  ]
};
