import { Component, Prop } from '@stencil/core';
import classNames from 'classnames/bind';

@Component({
  tag: 'lee-button',
  styleUrl: 'button.scss',
  shadow: true
})

export class Button {

  @Prop({ reflectToAttr: true })
  disabled: boolean;

  @Prop({ reflectToAttr: true })
  submit: boolean;

  @Prop({ reflectToAttr: true })
  primary: boolean;

  @Prop({ reflectToAttr: true })
  outline: boolean;

  @Prop()
  size: 'small' | 'default' | 'large' = 'default';

  render() {
    const type = this.submit ? 'submit' : 'button';

    var btnClass = classNames({
      btn: true,
      'btn-outline': this.outline,
      'btn-primary': this.primary,
      [`btn-${this.size}`]: true
    });

    return(
      <button type={type} class={btnClass} disabled={this.disabled}>
        <slot />
      </button>
    );
  }

}
