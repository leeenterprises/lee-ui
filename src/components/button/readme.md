# lee-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                               | Default     |
| ---------- | ---------- | ----------- | ---------------------------------- | ----------- |
| `color`    | `color`    |             | `"accent" \| "light" \| "primary"` | `'primary'` |
| `disabled` | `disabled` |             | `boolean`                          | `undefined` |
| `size`     | `size`     |             | `"default" \| "large" \| "small"`  | `'default'` |
| `type`     | `type`     |             | `"button" \| "reset" \| "submit"`  | `'button'`  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
