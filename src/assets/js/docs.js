const setValue = (property, value) => {
    if (value) {
        document.documentElement.style.setProperty(`--${property}`, value);
    }
};

const setBrand = options => {
    for (let option of Object.keys(options)) {
        const property = option;
        const value = options[option];
        setValue(property, value);
    }
}

const brandColors = {
  'qctimes' : {
    'brand-color-primary': '#006CB7',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#4091c9',
    'brand-color-secondary-contrast': '#202020',
    'brand-color-tertiary': '#697279',
    'brand-color-tertiary-contrast': '#ffffff'
  },
  'billingsgazette' : {
    'brand-color-primary': '#1e1e1e',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#ffcc00',
    'brand-color-secondary-contrast': '#1e1e1e',
    'brand-color-tertiary': '#1e1e1e',
    'brand-color-tertiary-contrast': '#ffffff'
  },
  'stl' : {
    'brand-color-primary': '#990000',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#c77373',
    'brand-color-secondary-contrast': '#202020',
    'brand-color-tertiary': '#202020',
    'brand-color-tertiary-contrast': '#ffffff'
  },
  'beatrice' : {
    'brand-color-primary': '#1C1C1A',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#FE762E',
    'brand-color-secondary-contrast': '#1C1C1A',
    'brand-color-tertiary': '#1C1C1A',
    'brand-color-tertiary-contrast': '#ffffff'
  },
  'muscatine' : {
    'brand-color-primary': '#018643',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#349e69',
    'brand-color-secondary-contrast': '#202020',
    'brand-color-tertiary': '#202020',
    'brand-color-tertiary-contrast': '#ffffff'
  },
  'pantagraph' : {
    'brand-color-primary': '#284b93',
    'brand-color-primary-contrast': '#ffffff',
    'brand-color-secondary': '#738ab9',
    'brand-color-secondary-contrast': '#202020',
    'brand-color-tertiary': '#202020',
    'brand-color-tertiary-contrast': '#ffffff'
  }
}

// Activate buttons
const dataBrandButtons = document.querySelectorAll('[data-brand]');
const dataThemeButtons = document.querySelectorAll('[data-theme]');
const dataCodeButtons = document.querySelectorAll('[data-code-toggle]');


for (let i = 0; i < dataBrandButtons.length; i++) {
    dataBrandButtons[i].addEventListener('click', () => {
        const brand = dataBrandButtons[i].dataset.brand;

        dataBrandButtons.forEach(function(button) {
          button.classList.remove('selected');
        });
        dataBrandButtons[i].classList.add('selected');
        setBrand(brandColors[brand]);
    })
}

for (let i = 0; i < dataThemeButtons.length; i++) {
    dataThemeButtons[i].addEventListener('click', () => {
        dataThemeButtons.forEach(function(button) {
          button.classList.remove('selected');
        });
        dataThemeButtons[i].classList.add('selected');

        const theme = dataThemeButtons[i].dataset.theme;
        const themeSheet = document.getElementById('theme-sheet');
        themeSheet.href = themeSheet.href.replace(/theme\-[\w\-]+/, theme);
    })
}

for (let i = 0; i < dataCodeButtons.length; i++) {
    dataCodeButtons[i].addEventListener('click', () => {
        dataCodeButtons[i].parentElement.classList.toggle('open');
    })
}

// Set initial brand values
setBrand(brandColors['qctimes']);
